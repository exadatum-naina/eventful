#!/bin/bash
#setting current date in variable
if [[ $# -eq 0 ]]
then
dt=$(date +'%Y-%m-%d')
else
dt=$1
fi
#assigning variable names
path_to_output_directory="/incoming/eventful/$dt"

hadoop fs -chmod 777 /home/ec2-user/code/eventful/
#uploading
hadoop fs -rm -r -skipTrash /incoming/eventful/$dt
hdfs dfs -mkdir $path_to_output_directory
hdfs dfs -mkdir $path_to_output_directory/event
hdfs dfs -mkdir $path_to_output_directory/venue
hdfs dfs -mkdir $path_to_output_directory/performer
hdfs dfs -copyFromLocal /home/ec2-user/code/eventful/data/event_data.xml.bz2 $path_to_output_directory/event
if [[ $? -eq 0 ]]
then
hive -hiveconf path_to_output_directory=$path_to_output_directory/event -f "/home/ec2-user/code/eventful/20161007/event.hql"
else
echo "Upload Event Command failed"
exit -1
fi

hdfs dfs -copyFromLocal /home/ec2-user/code/eventful/data/venue_data.xml.bz2 $path_to_output_directory/venue
if [[ $? -eq 0 ]]
then
hive -hiveconf path_to_output_directory=$path_to_output_directory/venue -f "/home/ec2-user/code/eventful/20161007/venue.hql"
else
echo "Upload Venue Command failed"
exit -1
fi

hdfs dfs -copyFromLocal /home/ec2-user/code/eventful/data/performer_data.xml.bz2 $path_to_output_directory/performer
if [[ $? -eq 0 ]]
then
hive -hiveconf path_to_output_directory=$path_to_output_directory/performer -f "/home/ec2-user/code/eventful/20161007/performer.hql"
else
echo "Upload Performer Command failed"
exit -1
fi
