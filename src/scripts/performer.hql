use edc_db;
add jar /home/ec2-user/code/eventful/20161007/hivexmlserde-1.0.5.3.jar;
drop table if exists edc_performers;
CREATE EXTERNAL TABLE edc_performers(
id STRING,
url     STRING,
name    STRING,
short_bio    STRING,
long_bio        STRING,
created STRING,
modified        STRING,
creator STRING,
demand_member_count     BIGINT,
event_count     BIGINT,
withdrawn    BIGINT,
withdrawn_note  STRING,
popularity    BIGINT,
image  STRUCT<url:STRING,width:BIGINT,height:BIGINT>,
links   ARRAY<MAP<STRING,STRING>
        >
)
ROW FORMAT SERDE 'com.ibm.spss.hive.serde2.xml.XmlSerDe'
WITH SERDEPROPERTIES (
"column.xpath.id"="/performer/id/text()",
"column.xpath.url"="/performer/url/text()",
"column.xpath.name"="/performer/name/text()",
"column.xpath.short_bio"="/performer/short_bio/text()",
"column.xpath.long_bio"="/performer/long_bio/text()",
"column.xpath.created"="/performer/created/text()",
"column.xpath.modified"="/performer/modified/text()",
"column.xpath.creator"="/performer/creator/text()",
"column.xpath.demand_member_count"="/performer/demand_member_count/text()",
"column.xpath.event_count"="/performer/event_count/text()",
"column.xpath.withdrawn"="/performer/withdrawn/text()",
"column.xpath.withdrawn_note"="/performer/withdrawn_note/text()",
"column.xpath.popularity"="/performer/popularity/text()",
"column.xpath.image"="/performer/image/*",
"column.xpath.links"="/performer/links/link/*"
)
STORED AS
INPUTFORMAT 'com.ibm.spss.hive.serde2.xml.XmlInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat'
LOCATION '${hiveconf:path_to_output_directory}'
TBLPROPERTIES (
"xmlinput.start"="<performer>",
"xmlinput.end"="</performer>"
);
