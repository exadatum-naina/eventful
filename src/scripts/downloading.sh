#!/bin/bash
#setting current date in variable
if [[ $# -eq 0 ]]
then
dt=$(date +'%Y%m%d')
else
dt=$1
fi
rm /home/ec2-user/code/eventful/data/event_data.xml.bz2
rm /home/ec2-user/code/eventful/data/venue_data.xml.bz2
rm /home/ec2-user/code/eventful/data/performer_data.xml.bz2
#assigning variable names
path_to_output_directory=$dt
hadoop jar /home/ec2-user/code/eventful/20161007/com.exadatum.edc.eventful-1.0-SNAPSHOT.jar Runner $path_to_output_directory
DIR="/home/ec2-user/code/eventful/data/"
# init
# look for empty dir 
if [ "$(ls -A $DIR)" ]; then
     echo "File Downloaded"
else
    echo "File Not Found"
    exit -1;
fi
