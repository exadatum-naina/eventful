use edc_db;
add jar /home/ec2-user/code/eventful/20161007/hivexmlserde-1.0.5.3.jar;
drop table if exists edc_venues;
CREATE EXTERNAL TABLE edc_venues(
id STRING,
url     STRING,
name     STRING,
description    STRING,
venue_type     STRING,
venue_display    BIGINT,
address  STRING,
city STRING,
tz_olson_path    STRING,
region  STRING,
region_abbr     STRING,
postal_code     BIGINT,
country STRING,
country_abbr2   STRING,
country_abbr    STRING,
latitude        STRING,
longitude    STRING,
geocode_type    STRING,
created STRING,     
modified        STRING,
owner   STRING,
withdrawn    STRING,
withdrawn_note  STRING,
popularity    BIGINT,
venue_capacity  BIGINT,
comments        STRING,
image  STRUCT<url:STRING,width:BIGINT,height:BIGINT>,
links   ARRAY<MAP<STRING,STRING>
        >
)
ROW FORMAT SERDE 'com.ibm.spss.hive.serde2.xml.XmlSerDe'
WITH SERDEPROPERTIES (
"column.xpath.id"="/venue/id/text()",
"column.xpath.url"="/venue/url/text()",
"column.xpath.name"="/venue/name/text()",
"column.xpath.description"="/venue/description/text()",
"column.xpath.venue_type"="/venue/venue_type/text()",
"column.xpath.venue_display"="/venue/venue_display/text()",
"column.xpath.address"="/venue/address/text()",
"column.xpath.city"="/venue/city/text()",
"column.xpath.tz_olson_path"="/venue/tz_olson_path/text()",
"column.xpath.region"="/venue/region/text()",
"column.xpath.region_abbr"="/venue/region_abbr/text()",
"column.xpath.postal_code"="/venue/postal_code/text()",
"column.xpath.country"="/venue/country/text()",
"column.xpath.country_abbr2"="/venue/country_abbr2/text()",
"column.xpath.country_abbr"="/venue/country_abbr/text()",
"column.xpath.latitude"="/venue/latitude/text()",
"column.xpath.longitude"="/venue/longitude/text()",
"column.xpath.geocode_type"="/venue/geocode_type/text()",
"column.xpath.created"="/venue/created/text()",
"column.xpath.modified"="/venue/modified/text()",
"column.xpath.owner"="/venue/owner/text()",
"column.xpath.withdrawn"="/venue/withdrawn/text()",
"column.xpath.withdrawn_note"="/venue/withdrawn_note/text()",
"column.xpath.popularity"="/venue/popularity/text()",
"column.xpath.venue_capacity"="/venue/venue_capacity/text()",
"column.xpath.comments"="/venue/comments/text()",
"column.xpath.image"="/event/image/*",
"column.xpath.links"="/venue/links/link/*"
)
STORED AS
INPUTFORMAT 'com.ibm.spss.hive.serde2.xml.XmlInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat'
LOCATION '${hiveconf:path_to_output_directory}'
TBLPROPERTIES (
"xmlinput.start"="<venue>",
"xmlinput.end"="</venue>"
);


