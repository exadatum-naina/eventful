use edc_db;
add jar /home/ec2-user/code/eventful/20161007/hivexmlserde-1.0.5.3.jar;
drop table if exists edc_events;
CREATE EXTERNAL TABLE edc_events(
id STRING,
url     STRING, 
title     STRING,
description    STRING,
start_time     STRING,
stop_time  STRING,
venue_id STRING,
all_day    BIGINT,
recur_string  STRING,
calendar_count     BIGINT, 
comment_count     BIGINT, 
going_count     BIGINT,
link_count     BIGINT,
created STRING,
owner   STRING,
privacy    BIGINT,
free        BIGINT,
price   BIGINT,
modified    STRING,
popularity BIGINT,
performers        STRING,
comments   STRING,
links   ARRAY<MAP<STRING,STRING>
        >,
image  STRUCT<url:STRING,width:BIGINT,height:BIGINT>,
categories   ARRAY<MAP<STRING,STRING     >
        >,
Subcategories     ARRAY<MAP<STRING,STRING   >
        >

)
ROW FORMAT SERDE 'com.ibm.spss.hive.serde2.xml.XmlSerDe'
WITH SERDEPROPERTIES (
"column.xpath.id"="/event/id/text()",
"column.xpath.url"="/event/url/text()",
"column.xpath.title"="/event/title/text()",
"column.xpath.description"="/event/description/text()",
"column.xpath.start_time"="/event/start_time/text()",
"column.xpath.stop_time"="/event/stop_time/text()",
"column.xpath.venue_id"="/event/venue_id/text()",
"column.xpath.all_day"="/event/all_day/text()",
"column.xpath.recur_string"="/event/recur_string/text()",
"column.xpath.calendar_count"="/event/calendar_count/text()",
"column.xpath.comment_count"="/event/comment_count/text()",
"column.xpath.going_count"="/event/going_count/text()",
"column.xpath.link_count"="/event/link_count/text()",
"column.xpath.created"="/event/created/text()",
"column.xpath.owner"="/event/owner/text()",
"column.xpath.privacy"="/event/privacy/text()",
"column.xpath.free"="/event/free/text()",
"column.xpath.price"="/event/price/text()",
"column.xpath.modified"="/event/modified/text()",
"column.xpath.popularity"="/event/popularity/text()",
"column.xpath.performers"="/event/performers/text()",
"column.xpath.comments"="/event/comments/text()",
"column.xpath.links"="/event/links/link/*",
"column.xpath.image"="/event/image/*",
"column.xpath.categories"="/event/categories/category/*",
"column.xpath.subcategories"="/event/subcategories/subcategory/*"
)
STORED AS
INPUTFORMAT 'com.ibm.spss.hive.serde2.xml.XmlInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat'
LOCATION '${hiveconf:path_to_output_directory}'
TBLPROPERTIES (
"xmlinput.start"="<event>",
"xmlinput.end"="</event>"
);


