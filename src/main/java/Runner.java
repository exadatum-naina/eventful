import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Logger;

/**
 * Created by naina on 10/5/16.
 */
public class Runner {
    public static void main(String[] args) {
        try {
            Logger logger = Logger.getLogger("Runner.class");
            Download downloadFile =new Download();
            logger.info("Started - Performer data download. " );
            downloadFile.saveFileFromUrlWithJavaIO("/home/ec2-user/code/eventful/data/performer_data.xml.bz2",
                    "http://static.eventful.com/images/export/exadatum-"+args[0]+"-full-performers.xml.bz2");
            logger.info("Completed - Performer data download ." );
            logger.info("Started - Event data download. " );
            downloadFile.saveFileFromUrlWithJavaIO("/home/ec2-user/code/eventful/data/event_data.xml.bz2",
                    "http://static.eventful.com/images/export/exadatum-"+args[0]+"-full-events.xml.bz2");
            logger.info("Completed - Event data download.  " );
            logger.info("Started - Venue data download.  " );
            downloadFile.saveFileFromUrlWithJavaIO("/home/ec2-user/code/eventful/data/venue_data.xml.bz2",
                    "http://static.eventful.com/images/export/exadatum-"+args[0]+"-full-venues.xml.bz2");
            logger.info("Completed - Venue data download. " );
            System.out.println("Download completed.");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
